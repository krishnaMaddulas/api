package com.javapractice.mapper;

import com.javapractice.entity.UserDetails;
import com.javapractice.model.UserDetailsVO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserDetailsMapper {

    UserDetailsMapper INSTANCE = Mappers.getMapper(UserDetailsMapper.class);

    @BeanMapping(nullValueMappingStrategy = RETURN_DEFAULT)
    UserDetailsVO domainToVoMapper(UserDetails userDetails);

    @BeanMapping(nullValueMappingStrategy = RETURN_DEFAULT)
    List<UserDetailsVO> domainListToVoList(List<UserDetails> userDetailsList);

    @BeanMapping(nullValueMappingStrategy = RETURN_DEFAULT)
    UserDetails voToDomain(UserDetailsVO userDetailsVo);

    @BeanMapping(nullValueMappingStrategy = RETURN_DEFAULT)
    List<UserDetails> voListToDomainList(List<UserDetailsVO> userDetailsVoList);
}
