package com.javapractice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @GetMapping(value = "/authenticate/{userId}")
    public String getAuthenticationResultById(@PathVariable("userId") int userId){
        try {
            if(userId%2 == 0){
                return "Valid User";
            }else{
                return "Invalid user";
            }
        }catch(Exception e){
            System.out.println("Issue authenticating the user");
            return "";
        }
    }
    @GetMapping(value = "/")
    public String defaultMethod(){
        return "login controller is responding";
    }

    @GetMapping(value = "/default/{value}")
    public String defaultMethod2(@PathVariable("value") String value){
        return "Default method is responding"+value;
    }
}

