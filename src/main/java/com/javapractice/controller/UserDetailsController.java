package com.javapractice.controller;

import com.javapractice.model.UserDetailsVO;
import com.javapractice.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/userDetails")
public class UserDetailsController {

    Logger logger = LoggerFactory.getLogger(UserDetailsController.class);

    @Autowired
    private UserServiceImpl userServiceImpl;

    @PostMapping("/save")
    public ResponseEntity<Object> addUserDetails(@RequestBody UserDetailsVO userDetailsVo){
        try {
            if(userDetailsVo != null){
                UserDetailsVO response = userServiceImpl.addUser(userDetailsVo);
                if(response != null){
                    return ResponseEntity.status(HttpStatus.OK).body(response);
                }else{
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Server unavailable");
                }
            }else{
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No data found in the request");
            }
        }catch (Exception e){
            logger.error("Unable to save the details", e);
            logger.error("Unable to save the details", e);

            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("Error reaching out server");
        }

    }
}

