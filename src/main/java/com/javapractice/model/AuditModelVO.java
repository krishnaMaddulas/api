package com.javapractice.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;

@Data
public class AuditModelVO {

    private String createdBy;

    private String createdDt;

    private String updatedBy;

    private String updatedDt;

    private String isDeleted;
}
