package com.javapractice.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
public class UserDetailsVO extends AuditModelVO{


    private String userId;

    private String firstName;

    private String lastName;

    private String password;

    private String userName;

}
