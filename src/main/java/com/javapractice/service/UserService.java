package com.javapractice.service;

import com.javapractice.entity.UserDetails;
import com.javapractice.model.UserDetailsVO;

public interface UserService {

    UserDetailsVO addUser(UserDetailsVO userDetails);
}
