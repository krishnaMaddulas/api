package com.javapractice.service.impl;

import com.javapractice.entity.UserDetails;
import com.javapractice.mapper.UserDetailsMapper;
import com.javapractice.model.UserDetailsVO;
import com.javapractice.repository.UserDetailsRepository;
import com.javapractice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDetailsRepository userDettailsRepo;


    @Override
    public UserDetailsVO addUser(UserDetailsVO userDetails) {
        if(userDetails != null){
            try{
                UserDetailsVO userDetailsVO = UserDetailsMapper.INSTANCE
                        .domainToVoMapper(userDettailsRepo.save(UserDetailsMapper.INSTANCE.voToDomain(userDetails)));
            }catch (Exception e){
          logger.error("Error saving the user details",e);
            }
        }
        return null;
    }
}
