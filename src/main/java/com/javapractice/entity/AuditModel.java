package com.javapractice.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;


@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AuditModel {

    @Column
    @CreatedBy
    private String createdBy;

    @Column
    @CreatedDate
    private String createdDt;

    @Column
    @LastModifiedBy
    private String updatedBy;

    @Column
    @LastModifiedDate
    private String updatedDt;

    @Column(length=1)
    private String isDeleted;
}
